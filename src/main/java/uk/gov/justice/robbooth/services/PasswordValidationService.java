package uk.gov.justice.robbooth.services;

public interface PasswordValidationService {
    boolean isValid(String password);
}
