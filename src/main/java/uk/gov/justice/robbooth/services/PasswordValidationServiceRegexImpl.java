package uk.gov.justice.robbooth.services;

import java.util.regex.Pattern;

public class PasswordValidationServiceRegexImpl implements PasswordValidationService{

    final Pattern pattern = Pattern.compile("((?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*[_])){9,}");
    @Override
    public boolean isValid(String password) {
        return pattern.matcher(password).find();
    }

}
