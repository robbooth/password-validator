package uk.gov.justice.robbooth.services;

public class PasswordValidationServiceImpl implements PasswordValidationService{

    @Override
    public boolean isValid(String password) {
        return (password.length() > 8)
                && (password.chars().anyMatch(Character::isUpperCase))
                && (password.chars().anyMatch(Character::isLowerCase))
                && (password.chars().anyMatch(Character::isDigit))
                && (password.contains("_"));
    }
}
