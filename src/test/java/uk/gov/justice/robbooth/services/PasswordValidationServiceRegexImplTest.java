package uk.gov.justice.robbooth.services;


import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;


public class PasswordValidationServiceRegexImplTest {

    private PasswordValidationServiceRegexImpl service;

    @BeforeEach
    public void setupService(){
        service = new PasswordValidationServiceRegexImpl();
    }


    @Test
    public void givenPassword_isEmptyString_thenIsValidReturnsFalse() {
        boolean result = service.isValid("");
        assertFalse(result);
    }

    @Test
    public void givenPassword_isNotMoreThan8Chars_thenIsValidReturnsFalse(){
        boolean result = service.isValid("password");
        assertFalse(result);
    }

    @Test
    public void givenPassword_doesNotContainCapitalLetter_thenIsValidReturnsFalse(){
        boolean result = service.isValid("abcdefghi");
        assertFalse(result);
    }

    @Test
    public void givenPassword_doesNotContainLowerCaseLetter_thenIsValidReturnsFalse(){
        boolean result = service.isValid("ABCDEFGHI");
        assertFalse(result);
    }

    @Test
    public void givenPassword_doesNotContainNumber_thenIsValidReturnsFalse(){
        boolean result = service.isValid("Abcdefghij");
        assertFalse(result);
    }

    @Test
    public void givenPassword_doesNotContainUnderscore_thenIsValidReturnsFalse(){
        boolean result = service.isValid("Abcdefgh1j");
        assertFalse(result);
    }

    @Test
    public void givenPassword_containsAllRequisiteChars_thenIsValidReturnsTrue(){
        boolean result = service.isValid("Abcdefgh1_");
        assertTrue(result);
    }
}