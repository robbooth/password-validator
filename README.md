# Robin Booth - Code Submission

This project implements a `PasswordValidationService` which validates a password in line with the goals outlined in the
[project brief](https://gist.github.com/Coupey/47cab3dd0a6bef84780c376907833746).

Project brief (copied) : 

---
### Goal
Design and implement some code that validates a password.

The password will be provided by the user (as an argument of the method) and should return if the password is valid or not.

A valid password should meet the following criteria:

* Have more than 8 characters
* Contains a capital letter
* Contains a lowercase
* Contains a number
* Contains an underscore

### Technical requirements:
* We want a method that answers if the password is valid or not.
* We do not need to know the reason that the password is invalid


---

### Implementation Details:
The service was created using a TDD approach with commits made when each of the individual validation criteria was 
applied. 

### Assumptions:
The password parameter passed as an argument to the isValid method is 
* a string
* not null

### Getting Started:
Prerequisite: 
1. Java 17 should be installed on the host machine.
2. Maven should be installed on the host machine.

### Setup:
Follow these steps to setup and run the project locally:
1. **Clone the Gitlab repository:** 
    ```bash
    git clone git@gitlab.com:robbooth/password-validator.git
    ```
   ```bash
   cd password-validator
   ```
2. **Run using Maven:**
   ```bash
   mvn test
   ```
   
Make sure you have [Maven](https://maven.apache.org/) installed on your machine.
